# compress-uncompressed

Simple tool for compressing all uncompressed files in btrfs directory

## Usage

```bash
compress-uncompressed -s /path/to/directory /path/to/file1 /path/to/file2
```

## Flags

| Flag | Description |
| ---- | ----------- |
| `-s` | Run `sync` after all operations |
| `-d` | Execute a dry run, skipping the actual compression step |
