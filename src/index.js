import _ from 'lodash';
import Lazy from 'lazy.js';
import path from 'path';
import fs from 'fs';
import { config } from './config.js';
import { debugLogStdout, debugLogStderr, isFile } from './util.js';
import { compressFile, isCompressed, printCompressionInfo } from './btrfs.js';
import { promisify } from 'util';
import { spawn } from 'child_process';
import { awaitTermination } from './process.js';
import sideEffect from '@mcoffin/side-effect';
import readline from 'readline';
import { asArray, stringifySafe } from './util.js';

async function doPrompt(s) {
    process.stdout.write(s);
    const rl = readline.createInterface({
        input: process.stdin,
    });
    const input = await new Promise((resolve, reject) => {
        rl.on('line', (s) => resolve(s));
        rl.on('error', (e) => reject(e));
    });
    rl.close();
    return input;
}

class InvalidBooleanValueError extends Error {
    constructor(value) {
        super(`Invalid boolean value: ${_.isUndefined(value) ? 'undefined' : value}`);
        this.value = value;
    }
}

async function doPromptBoolean(s, attempts = 0) {
    const validTrue = ['y', 'yes', 'true'];
    const validFalse = ['n', 'no', 'false'];

    function checkValid(v) {
        if (!_.isString(v)) {
            return false;
        }
        if (validTrue.includes(v)) {
            return true;
        } else if (validFalse.includes(v)) {
            return false;
        } else {
            throw new InvalidBooleanValueError(v);
        }
    }

    let input;
    let attemptCount = 0;
    do {
        try {
            attemptCount += 1;
            input = await doPrompt(s)
                .then((s) => s.toLowerCase());
            return checkValid(input);
        } catch (e) {
            if (attempts > 0 && attemptCount >= attempts) {
                throw e;
            }
            if (!(e instanceof InvalidBooleanValueError)) {
                throw e;
            }
            input = undefined;
        }
    } while (_.isUndefined(input));
}

function doSync() {
    debugLogStderr('syncing...');
    const child = spawn('sh', ['-c', 'set -e; sync;'], {
        stdio: ['ignore', 'inherit', 'inherit'],
    });
    return awaitTermination(child);
}

function printPrefixed(a, options = {}) {
    options = _.defaults(options, {
        prefix: "\t",
        logFn: console.log,
    });
    Lazy(asArray(a))
        .map(stringifySafe)
        .map((s) => options.prefix + s)
        .each((s) => options.logFn(s));
}

function statPromise(...args) {
    return promisify(fs.stat)(...args);
}

async function sortByModification(paths) {
    paths = await Promise.all(_.map(paths, (path) => statPromise(path).then((stats) => [path, stats])));
    return Lazy(paths)
        .sortBy(([path, stats]) => stats.mtime)
        .map(([path]) => path)
        .toArray();
}

async function main(config) {
    let paths = config.paths;
    if (paths.length < 1) {
        throw new Error('You must supply at least one path to compress');
    }
    if (config.get('sync', false)) {
        await doSync();
    }
    paths = await Promise.all(_.map(paths, async (p) => {
        try {
            return Lazy(await promisify(fs.readdir)(p, { withFileTypes: true }))
                .filter((entry) => entry.isFile())
                .map((entry) => path.join(p, entry.name))
                .toArray();
        } catch(e) {
            debugLogStderr('readdir error:', e);
            return (await isFile(p)) ? [p] : [];
        }
    }));
    paths = Lazy(paths).flatten().toArray();
    paths = await sortByModification(paths)
        .then((paths) => Lazy(paths));
    if (_.isNumber(config.maxCount)) {
        paths = paths.last(config.maxCount);
    }
    paths = paths.toArray();
    debugLogStderr(`paths[${paths.length}]:`);
    printPrefixed(paths, { logFn: debugLogStderr });
    const uncompressedPaths = await Promise.all(_.map(paths, async (p) => {
        if (await isCompressed(p)) {
            return undefined;
        }
        return p;
    }))
        .then((a) => _.filter(a, (p) => _.isString(p)));
    debugLogStderr('uncompessedPaths:', uncompressedPaths);
    debugLogStderr(`uncompressed: ${uncompressedPaths.length}/${paths.length}`);
    if (uncompressedPaths.length < 1) {
        console.log('Nothing to compress.');
        return;
    }
    if (config.shouldConfirm) {
        if (!await doPromptBoolean('continue [y/n]? ')) {
            console.log('Exiting due to user confirmation input');
            return;
        }
    }
    for (let p of uncompressedPaths) {
        debugLogStderr('compressing:', p);
        if (config.isDryRun) {
            console.log('Skipping compression due to dry run');
            continue;
        }
        await compressFile(p, config.compressionType);
    }
    if (config.get('sync', false)) {
        await doSync();
    }
    if (config.isVerbose && uncompressedPaths.length > 0) {
        console.log('compression info:');
        await printCompressionInfo(uncompressedPaths, process.stdout, { end: false });
    }
}

main(config);
