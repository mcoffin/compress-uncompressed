import _ from 'lodash';

export class ExitStatusError extends Error {
    constructor(code) {
        super(`child process terminated with exit status: ${code}`);
        this.exitStatus = code;
    }
}

export async function awaitTermination(child, options = {}) {
    options = _.defaultsDeep(_.cloneDeep(options), {
        checkStatus: true,
    });
    const exitStatus = await new Promise((resolve, reject) => {
        child.on('error', (e) => reject(e));
        ['close', 'end', 'exit'].forEach((evt) => {
            child.on(evt, (code) => resolve(code));
        });
    });
    if (options.checkStatus && exitStatus !== 0) {
        throw new ExitStatusError(exitStatus);
    }
    return exitStatus;
}
