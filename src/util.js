import _ from 'lodash';
import { promisify } from 'util';
import fs from 'fs';

function debugLog(logFn, ...args) {
    if (!_.isFunction(logFn)) {
        logFn = console.log;
    }
    return logFn(...args);
}

export function debugLogStdout(...args) {
    return debugLog(console.log, ...args);
}

export function debugLogStderr(...args) {
    return debugLog(console.error, ...args);
}

export async function awaitStreamDone(stream, successEvents = ['close', 'end']) {
    return await new Promise((resolve, reject) => {
        stream.on('error', (e) => reject(e));
        successEvents.forEach((evt) => {
            stream.on(evt, () => resolve());
        });
    });
}

export function isFile(p) {
    return promisify(fs.stat)(p)
        .then((stats) => stats.isFile())
        .catch(() => false);
}

export function asArray(v, options = {}) {
    if (_.isArray(v)) {
        return v;
    }
    options = _.defaults(options, {
        passthroughNull: false,
        passthroughUndefined: false,
    });
    if ((_.isUndefined(v) && !options.passthroughUndefined)|| (_.isNull(v) && options.passthroughNull)) {
        return [];
    }
    return [v];
}

export function stringifySafe(v) {
    if (_.isString(v)) {
        return v;
    }
    return `${_.isUndefined(v) ? 'undefined' : v}`;
}
