import _ from 'lodash';
import Lazy from 'lazy.js';
import fs from 'fs';
import { spawn } from 'child_process';
import readline from 'readline';
import { awaitTermination } from './process.js';
import { awaitStreamDone, debugLogStderr } from './util.js';

export async function isCompressed(p) {
    const child = spawn('sudo', ['compsize', p], {
        stdio: ['ignore', 'pipe', 'inherit'],
    });
    const rl = readline.createInterface({
        input: child.stdout,
    });
    let isReading = false;
    let lines = [];
    rl.on('line', (line) => {
        if (line.startsWith('TOTAL')) {
            isReading = true;
            return;
        } else if (!isReading) {
            return;
        }
        const [compressionType, percent] = Lazy(line.split(' '))
            .filter((v) => _.isString(v))
            .filter((s) => s.length > 0)
            .toArray();
        lines.push({
            'type': compressionType,
            percent,
        });
    });
    const rlDonePromise = awaitStreamDone(rl);
    const exitCode = await awaitTermination(child);
    await rlDonePromise;
    lines = _.filter(lines, (l) => l.type !== 'none' && l.type !== 'NONE');
    return lines.length > 0;
}

export async function compressFile(p, compressionType = 'zlib') {
    const child = spawn('sudo', ['btrfs', 'filesystem', 'defragment', `-c${compressionType}`, p], {
        stdio: ['ignore', 'inherit', 'inherit'],
    });
    return await awaitTermination(child);
}

export async function printCompressionInfo(p, stream, pipeOptions = {}) {
    pipeOptions = _.defaultsDeep(_.cloneDeep(pipeOptions), {
        end: true,
    });
    let args;
    if (_.isArray(p)) {
        args = ['compsize', ...p];
    } else if (_.isString) {
        args = ['compsize', p];
    } else {
        args = ['compsize', _.isUndefined(p) ? 'undefined' : `${p}`];
    }
    const child = spawn('sudo', args, {
        stdio: ['ignore', 'pipe', 'ignore'],
    });
    if (stream) {
        child.stdout.pipe(stream, pipeOptions);
    }
    return await awaitTermination(child);
}
