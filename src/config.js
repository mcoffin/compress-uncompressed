import _ from 'lodash';
import MinimistConfig from '@mcoffin/minimist-config';
import { asArray } from './util.js';

export default class Config extends MinimistConfig {
    constructor(args) {
        super({
            string: [
                'compression-type'
            ],
            boolean: [
                'dry-run',
                'sync',
                'verbose',
                'no-confirm'
            ],
            alias: {
                'compression-type': ['c'],
                'dry-run': ['d'],
                'sync': ['s'],
                'verbose': ['v'],
                'count': ['n'],
            },
            default: {
                'compression-type': 'zlib',
                'dry-run': false,
                'sync': false,
                'verbose': false,
                'confirm': true,
            },
        }, args);
    }

    get maxCount() {
        return this.get('count');
    }

    get shouldConfirm() {
        return this.get('confirm', true);
    }

    get isVerbose() {
        return this.get('verbose', false);
    }

    get isDryRun() {
        return this.get('dry-run', false);
    }

    get paths() {
        return asArray(this.trailing);
    }

    get compressionType() {
        return this.get('compression-type', 'zlib');
    }
}

export const config = new Config(process.argv.slice(2));
